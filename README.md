Used Articles
https://levelup.gitconnected.com/structure-your-react-redux-project-for-scalability-and-maintainability-618ad82e32b7
https://blog.scalac.io/react-redux-jwt-authentication.html
https://creativetimofficial.github.io/light-bootstrap-dashboard-react/#/user
https://github.com/creativetimofficial/light-bootstrap-dashboard-react

vscode user settings:
{
"git.autofetch": true,
"explorer.confirmDelete": false,
"files.autoSave": "afterDelay",
"explorer.confirmDragAndDrop": false,
"prettier.printWidth": 160,
"prettier.singleQuote": true,
"prettier.semi": false,
"prettier.trailingComma": "all",
"prettier.eslintIntegration": true,
"eslint.autoFixOnSave": true,
"editor.formatOnSave": true,
"files.trimTrailingWhitespace": true,
"files.insertFinalNewline": true,
"files.trimFinalNewlines": true,
}
